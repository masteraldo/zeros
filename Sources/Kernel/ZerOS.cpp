#include "ZerOS.h"
#include "..\VPU\EZ65C02.h"

//ZerOS Kernel instance
ZerOS Kernel;


void ZerOS::init()
{
	::init();

	pinMode(PIN_PB0, OUTPUT);
	Serial.begin(9600);

	//EZ65C02 Initialization
	VPU.init(DEFAULT_PC_START_ADDR);
};

void ZerOS::run()
{
   uint64_t start   = 0L;
   uint64_t elapsed = 0L;
   bool v = false;

   for(;;)
   {
	   digitalWrite(PIN_PB0, v);
	   v = !v;
	   start = millis();

	   for(int32_t i=1000000; i>=0;--i)
	   {
		   VPU.execCycle();
	   }

	   elapsed = millis() - start;
	   uint32_t hz = (uint32_t)(1000000.0f / (elapsed / 1000.0f));

	   Serial.printf("BRK Execution time for 1000000 cycles: %lu millis\n", elapsed);
	   Serial.printf("Average of %lu instructions per second\n", hz);

   }
};
