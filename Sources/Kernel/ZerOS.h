#ifndef SOURCES_KERNEL_ZEROS_H_
#define SOURCES_KERNEL_ZEROS_H_

#include <arduino.h>

class ZerOS
{
	public:
		void init();
		void run();
};

extern ZerOS Kernel;

#endif
